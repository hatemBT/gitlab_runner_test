FROM node:latest
WORKDIR /app
COPY package.json .
RUN npm install 
RUN npm audit fix --force
COPY . .
CMD ["node", "app.js"]
